# Zerotier Coma Fix

This standalone Go Program executes a fix/workaround for a know issue in ZeroTier, affectionately known as the "Coma bug". Specifics on the issue can be found here:

https://github.com/zerotier/ZeroTierOne/issues/1334

Whilst the root cause of this issue appears to have been fixed in 1.6.5 of ZeroTier One, some existing nodes may need this fix manually applied post update. That is where this nifty little tool comes in.

# Requirements

As a general user. You can download from the releases section of this repository. If you wish to build from source, you will need an installation of GoLang and modules as per the go.mod file specification. Do note this application is only compatible with Windows and has not been written for other operating systems as of yet.

# How to build from source

Assuming you have all the requirements required, run the following commands in order:

```
git clone git@gitlab.com:public-source-code/zerotier-coma-fix.git
cd ./zerotier-coma-fix
```

Commands for a 32 bit build:

```
GOOS=windows GOARCH=386 go build -o bin/zerotiercomafix.exe zerotiercomafix.go
```

Commands for a 64 bit build:

```
GOOS=windows GOARCH=amd64 go build -o bin/zerotiercomafix.exe zerotiercomafix.go
```

# How to use the software

Once you have a working executable either from downloading a release or by building your own. Simply copy the generated zerotiercomafix.exe file and paste on any affected Windows node. From there double click and follow any prompts. Including the UAC prompt to allow the program to gain admin privileges.
