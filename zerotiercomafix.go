package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"syscall"
	"time"

	"github.com/manifoldco/promptui"
	"golang.org/x/sys/windows"
)

func main() {
	DetectOSArchitecture()
	DetectIfWindowsAdmin()
	DetectZeroTierInstall()
	StopZeroTierService()
	DeletePeersdDirectory()
	CreateLocalConfFile()
	ResetNodeIdentity()
	StartZeroTierService()
	//Let the program sleep for 5 seconds to provide confirmation to user
	time.Sleep(5 * time.Second)
	os.Exit(0)
}

// Detect that OS is indeed Windows
func DetectOSArchitecture() {
	if runtime.GOOS == "windows" {
		fmt.Println("Windows OS Detected! Continuing...")
	} else {
		fmt.Println("Unsupported OS detected! Aborting...")
		os.Exit(1)
	}
}

// Detect if program is running with admin rights.
func DetectIfWindowsAdmin() {
	// if not elevated, relaunch by shellexecute with runas verb set.
	if !amAdmin() {
		runMeElevated()
	}
}

// Privilege escalation.
func runMeElevated() {
	verb := "runas"
	exe, _ := os.Executable()
	cwd, _ := os.Getwd()
	args := strings.Join(os.Args[1:], " ")

	verbPtr, _ := syscall.UTF16PtrFromString(verb)
	exePtr, _ := syscall.UTF16PtrFromString(exe)
	cwdPtr, _ := syscall.UTF16PtrFromString(cwd)
	argPtr, _ := syscall.UTF16PtrFromString(args)

	var showCmd int32 = 1 //SW_NORMAL

	err := windows.ShellExecute(0, verbPtr, exePtr, argPtr, cwdPtr, showCmd)
	if err != nil {
		fmt.Println(err)
	}
}

// Send a message to the console confirming admin status of the program.
func amAdmin() bool {
	_, err := os.Open("\\\\.\\PHYSICALDRIVE0")
	if err != nil {
		fmt.Println("Running under restricted privileges! Escalating...")
		return false
	}
	fmt.Println("Now running as Administrator! Continuing...")
	return true
}

// Detect if ZeroTier is even installed before doing anything otherwise abort.
func DetectZeroTierInstall() {
	if _, ZeroTier32InstallPath := os.Stat("C:/Program Files/ZeroTier/One"); os.IsNotExist(ZeroTier32InstallPath) {
		fmt.Println("Doesn't seem like a 32 Bit install of ZeroTier exists! Checking for 64 bit, if found I'll continue...")
	} else if _, ZeroTier64InstallPath := os.Stat("C:/Program Files/ZeroTier/One"); os.IsNotExist(ZeroTier64InstallPath) {
		fmt.Println("Doesn't seem like a 64 Bit install of ZeroTier exists!")
	} else {
		fmt.Println("ZeroTier Install cannot be found! Aborting!")
		os.Exit(1)
	}

}

// Stop ZeroTier One Service.
func StopZeroTierService() {
	fmt.Println("Stopping ZeroTier One Service...")
	cmd := exec.Command("net", "stop", "ZeroTierOneService")

	err := cmd.Run()

	if err != nil {
		log.Fatal(err)
	}
}

// Start ZeroTier One Service.
func StartZeroTierService() {
	fmt.Println("Starting ZeroTier One Service...")
	cmd := exec.Command("net", "start", "ZeroTierOneService")

	err := cmd.Run()

	if err != nil {
		log.Fatal(err)
	}
}

// Remove the Peers.d directory.
func DeletePeersdDirectory() {
	fmt.Println("Deleting Peers.d directory...")
	var PeersdDirectory = filepath.FromSlash("C:/ProgramData/ZeroTier/One/peers.d")

	err := os.RemoveAll(PeersdDirectory)
	if err != nil {
		fmt.Println(err)
	}
}

func ResetNodeIdentity() {
	var NodeIdentityPublicFilePath = "C:/ProgramData/ZeroTier/One/identity.public"
	var NodeIdentitySecretFilePath = "C:/ProgramData/ZeroTier/One/identity.secret"
	prompt := promptui.Prompt{
		Label:     "Did you need me to reset the Node ID? (CANNOT BE UNDONE!)",
		IsConfirm: true,
	}

	prompt.Run()
	// Remove identity.public file.
	NodeIdentityPublicErr := os.Remove(NodeIdentityPublicFilePath)
	if NodeIdentityPublicErr != nil {
		fmt.Println("It seems the identity.public file doesn't exist! Moving on...")
	} else {
		fmt.Println("identity.public file removed! Moving to remove identity.secret...")
	}
	// Remove identity.secret file.
	NodeIdentitySecretErr := os.Remove(NodeIdentitySecretFilePath)
	if NodeIdentitySecretErr != nil {
		fmt.Println("It seems the identity.secret file doesn't exist! Moving on...")
	} else {
		fmt.Println("identity.secret file removed! Continuing...")
	}
}

// Create the local.conf file and write a specific set of parameters to it.
func CreateLocalConfFile() {
	var LocalConfFileContents = "{ \n" +
		"\"settings\": { \n" +
		"	\"secondaryPort\": 21234 \n" +
		"	} \n" +
		"}"
	var LocalConfFilePath = "C:/ProgramData/ZeroTier/One/local.conf"
	// Make sure to always remove local.conf first before proceeding.
	err := os.Remove(LocalConfFilePath)
	if err != nil {
		os.Create("C:/ProgramData/ZeroTier/One/local.conf")
	} else {
		fmt.Println("Local.conf already exists! Moving on...")
	}
	f, err := os.Create("C:/ProgramData/ZeroTier/One/local.conf")
	if err != nil {
		fmt.Println(err)
		return
	}
	l, err := f.WriteString(LocalConfFileContents)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	fmt.Println(l, "bytes written successfully to local.conf")
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}
